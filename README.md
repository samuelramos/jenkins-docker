jenkins-docker
==============

## Build the docker image

Run the following command to build a new image. The `-t` flag tags the image so it's easier to find later using the `docker images` command:

```console
$ docker build -t docker-jenkins .
```

## Start the docker container

Running the image with `-d` runs the container in detached mode, leaving the container running in the background. The `-p` flag redirects a public port to a private port inside the container. Run the image previously built:

```
$ docker run -d \
  -p 10010:8080 \
  -p 10011:50000 \
  --name docker-jenkins \
  -e JAVA_OPTS="-Xmx2048m" -e JENKINS_OPTS="--handlerCountMax=300 --logfile=/var/log/jenkins/jenkins.log" \
  -v $(pwd)/app-data:/var/jenkins_home \
  -v $(pwd)/app-data:/var/log/jenkins \
  -v /var/run/docker.sock:/var/run/docker.sock \
  docker-jenkins:latest
```

Notes: 

a) In order to Jenkins have the ability to use docker, it's necessary to share the Docker from the host machine through the socket interface that Docker exposes. For that, include the following file `/var/run/docker.sock` in the command above.

b) When mounting the volume `/var/run/docker.sock` access permissions are overridden with permissions defined in the host machine for the same. Meaning that, changing file ownership during image build using `RUN touch /var/run/docker.sock && chown root:docker /var/run/docker.sock` as no effect when running the container. In some cases, access permissions - the pair user:group, can be the same. But for others, they may not even exist inside the container (e.g. unknown group identifier). In order to have user `jenkins` running docker commands (add the `jenkins` user to `docker` group - `RUN usermod -a -G docker jenkins`, won't give the user permissions to access the `/var/run/docker.sock` file) as solution/workaround, the user `jenkins` should be added to group `root` at the moment of the image build using `RUN usermod -a -G root jenkins`. 

### Using docker-compose

Build the docker images

```console
$ docker-compose build
```

Start the docker container

```console
$ docker-compose up
```
or, to keep the process running in background

```
docker-compose up -d 
```

Stop and destroy the docker containers

```
docker-compose stop
docker-compose rm
```

# Setting up the Jenkins

## Verifying that Jenkins is up and running

To inspect the startup result of Jenkins container it would be necessary to tail the log file `/var/log/jenkins/jenkins.log`.

Tail files from a running container, can be achieved by using the following command:

```console
$ docker exec -it [CONTAINER] tail -f [FILE]
```

Then:

```console
$ docker exec -it jenkins-docker_app_1 tail -f /var/jenkins_home/secrets/initialAdminPassword
```

## Setup Wizard [Jenkins]

Access the Jenkins web page using the URL:

```console
http://localhost:8080/
```

Enter the admin password that was generated in file `/var/jenkins_home/secrets/initialAdminPassword`.

Access files from a running container, can be achieved by using the following command:

```console
$ docker exec -it [CONTAINER] cat [FILE]
```

Then:

```console
$ docker exec -it docker-jenkins_app_1 cat /var/jenkins_home/secrets/initialAdminPassword
```

## Additional Plugins

* Locale Plugin (https://plugins.jenkins.io/locale/) 
* Bitbucket Plugin (https://plugins.jenkins.io/bitbucket)
* Blue Ocean Plugin (https://plugins.jenkins.io/blueocean/) 
* Docker plugin (https://plugins.jenkins.io/docker-plugin/)
* Xray for JIRA Jenkins Plugin v2.0.0 (https://confluence.xpand-it.com/display/public/XRAY/Integration+with+jenkins) (Note: JIRA Cloud Version)

Notes: 

a) First four, can be installed during the "Setup Wizard \[Jenkins\]"

### Manual installation

#### Locale (plugin for Jenkins always show english text)

1. In home of Jenkins, click in `Manage Jenkins`
2. Click in `Manage Plugins`
3. In Plugin manager, click in `Available` tab and after load the tab, search the plugins
4. Search for Locale, mark the checkbox and click in `Install` button

Configuration:

1. After install Locale, go to `Home > Manage Jenkins > Configure System`
2. Search for `Locale` section
3. On the textinput `Default Language` enter `en` and check `Ignore browser preference and force this language to all users`
4. Click `Save`

#### BitBucket (plugin to integrate with a GIT repository)

1. In home of Jenkins, click in `Manage Jenkins`
2. Click in `Manage Plugins`
3. In Plugin manager, click in `Available` tab and after load the tab, search the plugins
4. Search for Bitbucket, mark the checkbox and click in `Install` button

#### Blue Ocean (plugin to improve the usability of Jenkins and focus in pipelines scripts)

1. In home of Jenkins, click in `Manage Jenkins`
2. Click in `Manage Plugins`
3. In Plugin manager, click in `Available` tab and after load the tab, search the plugins
4. Search for `Blue Ocean`, mark the checkbox and click in `Install` button

#### Xray for JIRA Jenkins (plugin to integrate with Xray for JIRA)

1. In home of Jenkins, click in `Manage Jenkins`
2. Click in `Manage Plugins`
3. In Plugin manager, click in `Advanced` tab and after load the tab, go to `Upload Plugin` section
4. Click `Choose file` and navidate to `app-data-reference/plugins/xray-for-jira-connector` directory, select the file `xray-for-jira-connector.hpi` and click in `Upload` button

Configuration:

1. After install Xray, go to `Home > Manage Jenkins > Configure System`
2. Search for `Xray for JIRA configuration` section
3. Enter the JIRA configuration (the `Configuration ID` identifies the JIRA instance that should be used by a Jenkins job)
4. Click `Test Connection` to check if it works
5. Click `Save`

Notes: 

a) Plugin configuration should be stored in `app-data/com.xpandit.plugins.xrayjenkins.model.ServerConfiguration.xml` file.

#### Docker plugin (plugin that allows to use a docker host to dynamically provision build agents)

1. In home of Jenkins, click in `Manage Jenkins`
2. Click in `Manage Plugins`
3. In Plugin manager, click in `Available` tab and after load the tab, search the plugins
4. Search for `Docker plugin`, mark the checkbox and click in `Install` button


## Jenkins Slaves

### Using a Docker host to dynamically provision build agents

For Docker host, can be used the Docker local installation, a dedicated host (e.g. Kubernetes) or a virtual machine with Docker installed.

With a Docker host in place, and after installing the `Docker plugin` in Jenkins, follow the instructions to configure a new Docker host.

1. Go to `Home > Manage Jenkins > Manage Nodes and Clouds`
2. Select the `Configure Clouds` menu
3. Click `Add a new cloud` and select `Docker`
4. Give a `Name` that will identify the cloud (e.g. `vagrant-sandbox-docker`)
5. Click `Docker Cloud Details...`
6. Fill the `Docker Host URI` with the hostname that identifies the Docker cloud (add the `Server credential` if necessary)
7. Select the `Enabled` checkbox
8. Set the `Container Cap` to a maximum of `5`
9. Click `Docker Agent Templates...` and then `Add Docker Template`
10. Give a name to the `Label` that will identify the template (e.g. java-11)
11. Select the `Enabled` checkbox
12. Give a `Name` that will prefix the node name identifier
13. Enter the `Docker image` identifier
14. In the `Remote File System Root` insert the Jenkins home directory path in the docker container (e.g. `/home/jenkins`)
15. For `Usage` select `Only build jobs with label expressions matching this node`
16. Set `Idle timeout` to `10`
17. In the `Connect method` select `Connect with SSH`, in the `SSH key` select `Inject SSH Key` and for `User` enter the name of the user to login into the docker container (e.g. `jenkins`)
18. Under the `Node properties`, click `Add Node Property`, select `Environment Variables` and then in `List of variables` click `Add`
19. In `Name` enter `JAVA_HOME` and in `Value` use the Java home directoty path in the docker container (e.g. `/usr/local/openjdk-11`)
20. Click `Apply`
21. Click `Save`

Notes: 

a) To test the connectivity between machines - Jekins master and the Docker host, in a command line from the Jenkins master machine execute the following command. 

```
curl http://<docker-host-uri>:4243/version
```

The result should be something like this:

```
{"Platform":{"Name":"Docker Engine - Community"},"Components":[{"Name":"Engine","Version":"19.03.7","Details":{"ApiVersion":"1.40","Arch":"amd64","BuildTime":"2020-03-04T01:22:45.000000000+00:00","Experimental":"false","GitCommit":"7141c199a2","GoVersion":"go1.12.17","KernelVersion":"3.10.0-1062.12.1.el7.x86_64","MinAPIVersion":"1.12","Os":"linux"}},{"Name":"containerd","Version":"1.2.13","Details":{"GitCommit":"7ad184331fa3e55e52b890ea95e65ba581ae3429"}},{"Name":"runc","Version":"1.0.0-rc10","Details":{"GitCommit":"dc9208a3303feef5b3839f4323d9beb36df0a9dd"}},{"Name":"docker-init","Version":"0.18.0","Details":{"GitCommit":"fec3683"}}],"Version":"19.03.7","ApiVersion":"1.40","MinAPIVersion":"1.12","GitCommit":"7141c199a2","GoVersion":"go1.12.17","Os":"linux","Arch":"amd64","KernelVersion":"3.10.0-1062.12.1.el7.x86_64","BuildTime":"2020-03-04T01:22:45.000000000+00:00"}
```

If the above command doesn't work, it's necessary to enable Docker Remote API in the Docker host configuration. Namely, expose Docker’s TCP port (`tcp://0.0.0.0:4243`).

b) If using a Docker local installation in MAC OS X, it's possible to face some dificulties changing Docker configuration to enable Docker Remote API. As alternative, can be used a virtual machine. In the repository https://bitbucket.org/samuelramos/vagrant-sandbox/ it's possible to find the provisioning of a virtual machine (with Docker) using Vagrant.

c) As Docker image, should be used a SSH Slave image. Please check the Docker Hub (https://hub.docker.com/r/jenkins/ssh-slave) and/or the section "Docker images for SSH Slaves"

### Using statically hosted build agents

For hosting build agents, can be used a dedicated machine, a virtual machine or a Docker host (local installation, a dedicated host (e.g. Kubernetes) or a virtual machine with Docker installed)

Follow the instructions to configure the new build agent.

1. Generate a public/private rsa key pair for the agent
2. Start the SSH Slave (should have the SSH enabled)
3. With a SSH Slave up and running, add the public key to `/home/jenkins/.ssh/authorized_keys`
4. Then, in Jenkins master go to `Home > Manage Jenkins > Manage Nodes and Clouds`
5. Select the `New Node` menu
6. Give a `Name` that will identify the build agent
7. Select `Permanent Agent`
8. Click `OK`
9. In the `Remote root directory` insert the Jenkins home directory path in the ssh slave (e.g. `/home/jenkins`)
10. Give a name to the `Label` that will identify the template (e.g. `java-8`)
15. For `Usage` select `Only build jobs with label expressions matching this node`
16. In the `Launch method` select `Launch agents via SSH`
17. Fill the `Host` with the hostname that identifies the SSH Slave
18. For `Credentials` click `Add` and then click `Jenkins` that will open a `Jenkins Credentials Provider: Jenkins` dialog
19. In the dialog, for the `Kind` select `SSH Username with private key`, fill `ID`and `Description` with `ssh-slave`, for `User` enter the name of the user to login into the SSH Slave (e.g. `jenkins`), in `Private Key` select `Enter Directly` and enter the private key
20. Click `Add` to save and close the dialog
21. Then in `Credntials` select the created credentials (e.g. `jenkins (ssh-slave)`)
22. In `Host Key Verification Strategy` select the `Manually trusted key Verification Strategy`
18. Under the `Node properties`, select `Environment Variables` and then in `List of variables` click `Add`
19. In `Name` enter `JAVA_HOME` and in `Value` use the Java home directoty path in the docker container (e.g. `/usr/local/openjdk-8`)
20. Click `Apply`
21. Click `Save`

Notes:

a) The demo in this repository, under the folder `master-node-and-ssh-slaves-nodes`, considered the usage of a local installaton of Docker. Jenkins master and SSH Slaves are provisioned by a docker-compose script.

b) Regarding SSH Slave images please check the Docker Hub (https://hub.docker.com/r/jenkins/ssh-slave) and/or the section "Docker images for SSH Slaves".

b) When using SSH Slave Docker image, the public key should be passed as an environment variable. Automatically, the public key will be added to 
 `/home/jenkins/.ssh/authorized_keys`

#### ssh-slave

```
$ ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/Users/samuelramos/.ssh/id_rsa): ssh-slave-id_rsa
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in ssh-slave-id_rsa.
Your public key has been saved in ssh-slave-id_rsa.pub.
The key fingerprint is:
SHA256:Vf54PogcQ/fb721ZBRsuKpkazk8+nGB0ypXUt/EZSC8 samuelramos@Samuels-MacBook-Pro.local
The key's randomart image is:
+---[RSA 2048]----+
|         . .o.   |
|        . .o+.+  |
|       . .o.E=.* |
|      . +o .o== .|
|     o +Soo...+ .|
|      * +..+ + o.|
|     + =.oo . + +|
|      +o+      o+|
|       .o.     o+|
+----[SHA256]-----+
```

#### ssh-slave-docker

```
$ ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/Users/samuelramos/.ssh/id_rsa): ssh-slave-docker-id_rsa
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in ssh-slave-docker-id_rsa.
Your public key has been saved in ssh-slave-docker-id_rsa.pub.
The key fingerprint is:
SHA256:x3E6FaF0pt4OYbJBc+pvUDYIv9RCZvjnLM+2tHtQneM samuelramos@Samuels-MacBook-Pro.local
The key's randomart image is:
+---[RSA 2048]----+
|      ..* o =.   |
|      .B B = .   |
|       .O @ o. . |
|       o.%.O. +  |
|        S+B... . |
|        .+++  E  |
|         ++..    |
|         o+..    |
|         .++     |
+----[SHA256]-----+
```

#### ssh-slave-java-8

```
$ ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/Users/samuelramos/.ssh/id_rsa): ssh-slave-java-8-id_rsa
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in ssh-slave-java-8-id_rsa.
Your public key has been saved in ssh-slave-java-8-id_rsa.pub.
The key fingerprint is:
SHA256:BOUqVqN7frWf94lubJdm6P02FGAB46Cj4vaA25G6Y/g samuelramos@Samuels-MacBook-Pro.local
The key's randomart image is:
+---[RSA 2048]----+
|      .... o...  |
|       o. o .o   |
|      ooo  .. .  |
|     o.+.      . |
|   .+.. S       .|
|  o.oo    .    . |
|.. *. .  . .. o .|
|.o= +o  . .  *+*o|
|.=E. ...   .B=+==|
+----[SHA256]-----+
```

#### ssh-slave-python

```
$ ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/Users/samuelramos/.ssh/id_rsa): ssh-slave-python-id_rsa
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in ssh-slave-python-id_rsa.
Your public key has been saved in ssh-slave-python-id_rsa.pub.
The key fingerprint is:
SHA256:jfkWy3AuGchdIktwEb7EAauYbmAfg+JOp1wMcuaOmTs samuelramos@Samuels-MacBook-Pro.local
The key's randomart image is:
+---[RSA 2048]----+
|    o.=o         |
|     * .         |
|    . * . .      |
| o.. + * *       |
|*++o  = S +      |
|B++ o    O o     |
| =.=    o *      |
|E=+      o       |
|=*.              |
+----[SHA256]-----+
```

## Docker images for SSH Slaves 

### Building a ssh-slave-docker image

Run the following command:

```
$ docker build -t ssh-slave-docker ssh-slave-docker/.
```

#### Test the image

```
$ docker run -d \
  -v /var/run/docker.sock:/var/run/docker.sock \
  ssh-slave-docker
```

#### Upload image to docker hub

```
$ docker tag ssh-slave-docker samuelramos/ssh-slave-docker:latest
$ docker push samuelramos/ssh-slave-docker:latest
```

### Building a ssh-slave-java-8 image

Run the following command:

```
$ docker build -t ssh-slave-java-8 ssh-slave-java-8/.
```

#### Test the image

```
$ docker run -d ssh-slave-java-8
```

#### Upload image to docker hub

```
$ docker tag ssh-slave-java-8 samuelramos/ssh-slave-java-8:latest
$ docker push samuelramos/ssh-slave-java-8:latest
```

### Building a ssh-slave-java-11 image

Run the following command:

```
$ docker build -t ssh-slave-java-11 ssh-slave-java-11/.
```

#### Test the image

```
$ docker run -d ssh-slave-java-11
```

#### Upload image to docker hub

```
$ docker tag ssh-slave-java-11 samuelramos/ssh-slave-java-11:latest
$ docker push samuelramos/ssh-slave-java-11:latest
```

### Building a ssh-slave-python image

Run the following command:

```
$ docker build -t ssh-slave-python ssh-slave-python/.
```

#### Test the image

```
$ docker run -d ssh-slave-python
```

#### Upload image to docker hub

```
$ docker tag ssh-slave-python samuelramos/ssh-slave-python:latest
$ docker push samuelramos/ssh-slave-python:latest
```

### Building a ssh-slave-selenium image

Run the following command:

```
$ docker build -t ssh-slave-selenium ssh-slave-selenium/.
```

#### Test the image

```
$ docker run -d ssh-slave-selenium
```

#### Upload image to docker hub

```
$ docker tag ssh-slave-selenium samuelramos/ssh-slave-selenium:latest
$ docker push samuelramos/ssh-slave-selenium:latest
```

## Running a Jenkins Job

### Samples

#### Pipeline project 

1. Stop Jenkins
2. Copy the `pipeline-sample` folder from the `app-data-referecence/jobs/` directory to Jenkins volume data directory `app-data/jobs/`
3. Start Jenkins again
4. After Jenkins started, go to `pipeline-sample > Configure > Pipeline`
5. Enter the SCM credentials (user/password)
6. Save the changes and the job should be ready to use

#### Xray project 

[...]

## References

Docker & Jenkins: Data That Persists

From <https://engineering.riotgames.com/news/docker-jenkins-data-persists>

Quick start with Jenkins in docker

From <https://medium.com/@gustavo.guss/quick-tutorial-of-jenkins-b99d5f5889f2>

Jenkins Starting with Pipeline doing a Node.js test

From <https://medium.com/@gustavo.guss/jenkins-starting-with-pipeline-doing-a-node-js-test-72c6057b67d4>

Jenkins Building Docker Image and Sending to Registry

From <https://medium.com/@gustavo.guss/jenkins-building-docker-image-and-sending-to-registry-64b84ea45ee9>

Jenkins Master and Slaves as Docker Containers

From <https://rafaelnexus.com/tutorials/jenkins-master-and-slaves-as-docker-containers/>

How to enable Docker remote API

From <https://rafaelnexus.com/tutorials/how-to-enable-docker-remote-api/>

Configure and troubleshoot the Docker daemon

From <https://docs.docker.com/config/daemon/>

Jenkins Configure Master and Slave Nodes

From <https://dzone.com/articles/jenkins-03-configure-master-and-slave>
 
How to add a new slave node to Jenkins

From <http://yallalabs.com/devops/how-to-add-linux-slave-node-agent-node-jenkins/>

Pipeline Syntax

From <https://jenkins.io/doc/book/pipeline/syntax/>