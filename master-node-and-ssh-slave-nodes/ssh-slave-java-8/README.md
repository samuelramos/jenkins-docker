ssh-slave-java-8
================

Sources:

Jenkins SSH Build Agent Docker image

Docker Hub
From <https://hub.docker.com/r/jenkins/ssh-slave>

GitHub
From <https://github.com/jenkinsci/docker-ssh-slave>